package main

import (
	"fmt"
	"net"
	"sync"
)

// this one used as testing
func main() {
	iface, _ := net.InterfaceByName("wlan0")
	// PoisonThroughSelfByIp(net.ParseIP("10.244.31.254"), net.ParseIP("10.244.2.69"), iface)
	// setIPTables()
	// go startTargetsWebserver()
	for device := range scan(iface) {
		fmt.Println(device.ip)
	}
	wg := new(sync.WaitGroup)
	wg.Add(1)
	// CleanPoisonByIp(net.ParseIP("192.168.0.1"), net.ParseIP("192.168.0.13"), iface, wg)
	wg.Wait()
}
