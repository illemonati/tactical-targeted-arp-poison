module gitlab.com/illemonati/tactical-targeted-arp-poison

go 1.13

require (
	github.com/coreos/go-iptables v0.4.3
	github.com/gin-gonic/gin v1.5.0
	github.com/mdlayher/arp v0.0.0-20191213142603-f72070a231fc
)
