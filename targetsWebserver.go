package main

import (
	"github.com/coreos/go-iptables/iptables"
	"github.com/gin-gonic/gin"
	"os"
)

func setIPTables() {
	tables, err := iptables.New()
	if err != nil {
		return
	}
	tables.AppendUnique("nat", "PREROUTING", "-ptcp", "--dport=80", "-jDNAT", "--to-destination=192.168.0.6:180")
	tables.AppendUnique("nat", "PREROUTING", "-ptcp", "--dport=443", "-jDNAT", "--to-destination=192.168.0.6:1443")
}

func startTargetsWebserver() {
	if _, err := os.Stat("logs/"); os.IsNotExist(err) {
		os.Mkdir("logs/", os.ModePerm)
	}
	file, _ := os.Create("logs/targetsWebserverLog.txt")
	// print(err.Error())
	r := gin.New()
	r.Use(gin.LoggerWithWriter(file))
	r.Any("/*path", func(c *gin.Context) {
		r.LoadHTMLFiles("assets/targetsWebserver/index.html")
		c.HTML(200, "index.html", gin.H{})
	})
	go r.Run(":180")
	go r.RunTLS(":1443", "./assets/ssl/c.crt", "./assets/ssl/k.pem")
}
