package main

import (
	"encoding/binary"
	"net"
	"time"

	"github.com/mdlayher/arp"
)

type Device struct {
	ip  net.IP
	mac net.HardwareAddr
}

func scan(iface *net.Interface) chan Device {
	results := make(chan Device)
	go func() {
		var network *net.IPNet
		if addrs, err := iface.Addrs(); err != nil {
			close(results)
			return
		} else {

			for _, addr := range addrs {
				if ipnet, ok := addr.(*net.IPNet); ok {
					if ipv4 := ipnet.IP.To4(); ipv4 != nil {
						network = &net.IPNet{
							IP:   ipv4,
							Mask: ipnet.Mask[len(ipnet.Mask)-4:],
						}
					}
				}
			}
		}

		if network == nil {
			close(results)
			return
		} else if network.IP[0] == 127 {
			close(results)
			return
		}
		// fmt.Println(network)
		networkIPs := ips(network)
		// fmt.Println(networkIPs)
		for _, ip := range networkIPs {
			go func() {
				client, err := arp.Dial(iface)
				client.SetDeadline(time.Now().Add(time.Second * 2))
				if err != nil {
					close(results)
					return
				}
				mac, err := client.Resolve(ip)
				if err == nil {
					device := Device{ip, mac}
					results <- device
				}
			}()
			time.Sleep(200 * time.Millisecond)
		}
		close(results)
		return
	}()
	return results
}

// taken from https://github.com/google/gopacket/blob/master/examples/arpscan/arpscan.go
func ips(n *net.IPNet) (out []net.IP) {
	num := binary.BigEndian.Uint32([]byte(n.IP))
	mask := binary.BigEndian.Uint32([]byte(n.Mask))
	num &= mask
	for mask < 0xffffffff {
		var buf [4]byte
		binary.BigEndian.PutUint32(buf[:], num)
		out = append(out, net.IP(buf[:]))
		mask++
		num++
	}
	return
}
